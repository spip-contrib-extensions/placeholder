<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function placeholder_quete_logo_objet($flux) {
	if (empty($flux['data'])) {
		include_spip('placeholder_fonctions');
		
		if ($chemin = placeholder_chercher(array('extension'=>'jpg'))) {
			$flux['data'] = array(
				'chemin' => $chemin,
				'fichier' => basename($chemin),
				'timestamp' => @filemtime($chemin),
			);
		}
	}
	
	return $flux;
}

function placeholder_declarer_tables_interfaces($flux) {
	$flux['table_des_traitements']['FICHIER'][0] = 'placeholder_document(get_spip_doc(%s))';
	$flux['table_des_traitements']['URL_DOCUMENT'][0] = 'placeholder_document(%s)';
	
	return $flux;
}
