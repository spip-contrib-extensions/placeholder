
# Placeholder

![Placeholder](placeholder.svg)

Remplace les images manquantes que SPIP ne trouve pas, que ce soit logos ou documents (et ce quelque soit le type de fichier, images, pdf, etc).

Ce plugin est surtout utile quand on récupère un site existant en local pour développer sans récupérer le dossier IMG qui peut parfaitement faire 15Go. On ne va pas récupérer ça à chaque fois qu'on veut développer un site existant, et pourtant on veut que des images s'affichent à la taille voulue, passe par |image_recadre, Adaptive, etc. Sinon ça casse l'affichage.

## Fonctionnement

Quand un logo ou un document n'est pas trouvé, le plugin va chercher un document de même extension dans le chemin du path `placeholder/placeholder.XXX`.

## Limites

Il se peut que la recherche ne soit pas vraiment optimisée pour le moment, et que cela engendre des lenteurs avec l'activation de ce plugin. Si quelqu'un sait mieux faire…
