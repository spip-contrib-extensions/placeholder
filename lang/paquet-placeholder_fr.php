<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'placeholder_description' => 'Remplace les images manquantes dans SPIP par une image générique',
	'placeholder_slogan' => 'Remplace les images manquantes',
);
