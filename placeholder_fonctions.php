<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Traitement pour les fichiers des documents de Médias
function placeholder_document($fichier) {
	// Seulement si le fichier n'existe pas
	if (!file_exists($fichier)) {
		// Il peut s'agire d'URL absolue, donc on s'assure de ne récupérer que le morceau voulu
		$fichier = parse_url($fichier, PHP_URL_PATH);
		
		// On vire le IMG
		$fichier = str_replace(_DIR_IMG, '', $fichier);
		
		// On cherche si ce fichier existait dans les documents et avec quelles infos
		if (!$infos = sql_fetsel('extension, largeur, hauteur', 'spip_documents', 'fichier ='.sql_quote($fichier))) {
			$infos = array('extension' => pathinfo($fichier, PATHINFO_EXTENSION));
		}
		
		$fichier = placeholder_chercher($infos);
	}
	
	return $fichier;
}

/**
 * Fonction centrale de recherche de placeholder suivant les infos de fichier
 * 
 * @param array $infos
 *   Infos sur le fichier à remplacer : extension, largeur, hauteur
 * @return string
 *   Retourne le chemin d'un fichier placeholder
 */
function placeholder_chercher($infos=array()) {
	static $placeholders = array();
	$hash = '';
	$hash .= $infos['extension'] ?? '';
	$hash .= isset($infos['largeur']) ? '_'.$infos['largeur'].'x' : '';
	$hash .= $infos['hauteur'] ?? '';
	
	if (!isset($placeholders[$hash])) {
		$placeholders[$hash] = '';
		if (!empty($infos['extension'])) {
			// On cherche si on fonction est dédiée à cette extension
			if ($fonction_extension = charger_fonction($infos['extension'], 'placeholder', true)) {
				$placeholders[$hash] = $fonction_extension($infos);
			}
			// Sinon on cherche un fichier dédié à cette extension
			elseif ($placeholder_extension = find_in_path('placeholder/placeholder.'.$infos['extension'])) {
				$placeholders[$hash] = $placeholder_extension;
			}
		}
		
		// Sinon un truc bidon par défaut
		if (!$placeholders[$hash]) {
				$placeholders[$hash] = find_in_path('placeholder/defaut.png');
		}
	}
	
	return $placeholders[$hash];
}
